package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseOpratelWeb;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class OpratelWeb extends TestBaseOpratelWeb {
	
	final WebDriver driver;
	public OpratelWeb(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInOpratelWeb(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Web Opratel - landing");
		
		
			espera(500);
					String elemento1 = driver.findElement(By.xpath("//*[@id=\"top\"]/div/div/div[3]/h2")).getText();
					Assert.assertEquals(elemento1,"Mobile Entertainment & Monetization");
					System.out.println(elemento1);
			espera(500);
		
			
	//Menu Section About US		
			WebElement menuSection2 = driver.findElement(By.xpath("//a[contains(text(), 'About Us')]"));
				menuSection2.click();
					String elemento2 = driver.findElement(By.xpath("//*[@id=\"who\"]/div/div[1]/div")).getText();
					Assert.assertEquals(elemento2,"About Us");
					System.out.println(elemento2);
			espera(500);
			
			
	//Menu Section Why Opratel		
			espera(500);
				WebElement menuSection3 = driver.findElement(By.xpath("//a[contains(text(), 'Why Opratel')]"));
				menuSection3.click();
					String elemento3 = driver.findElement(By.xpath("//*[@id=\"why\"]/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento3,"Why Opratel");
					System.out.println(elemento3);
			espera(500);
			
		
	//Menus Section Markets
			espera(500);
				WebElement menuSection4 = driver.findElement(By.xpath("//a[contains(text(), 'Markets')]"));
				menuSection4.click();
					String regionA = driver.findElement(By.xpath("//*[@id=\"markets\"]/div/div/div[2]/div[1]/h3")).getText();
					Assert.assertEquals(regionA,"  Central America");
					System.out.println(regionA);
					
					String regionB = driver.findElement(By.xpath("//*[@id=\"markets\"]/div/div/div[2]/div[2]/h3")).getText();					
					Assert.assertEquals(regionB,"  South America");
					System.out.println(regionB);
					
					String regionC = driver.findElement(By.xpath("//*[@id=\"markets\"]/div/div/div[2]/div[3]/h3")).getText();					
					Assert.assertEquals(regionC,"  África");
					System.out.println(regionC);
			espera(500);
		
			
	//Menu Section Our Products			
			espera(500);
				WebElement menuSection5 = driver.findElement(By.xpath("//a[contains(text(), 'Our Products')]"));
				menuSection5.click();
					String elemento5 = driver.findElement(By.xpath("//*[@id=\"products\"]/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento5,"Our Products");
					System.out.println(elemento5);
			espera(500);
			
			
	//Menu Section Our Partners		
			espera(500);
			WebElement menuSection6 = driver.findElement(By.xpath("//a[contains(text(), 'Our Partners')]"));
			menuSection6.click();
				String elemento6 = driver.findElement(By.xpath("//*[@id=\"partners\"]/div/div[1]/div/h2")).getText();
				Assert.assertEquals(elemento6,"Partners");
				System.out.println(elemento6);
			espera(500);
		
	//Menu Section Contact Us			
			WebElement menuSection7 = driver.findElement(By.xpath("//a[contains(text(), 'Contact us')]"));
			menuSection7.click();
				String elemento7 = driver.findElement(By.xpath("//*[@id=\"contact-form\"]/div[1]/label")).getText();
				Assert.assertEquals(elemento7,"Name and last name");
				System.out.println(elemento7);
			espera(500);
		
		
		
			espera(500);

		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Web Opratel"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Web Opratel - Landing");
			
	}			

	
}  

